# creates a giant matrix for R to do calculations on

require 'json'

evc = File.open("eigen-sorted.txt").readlines
deg = File.open("degree-sorted.txt").readlines
btw = File.open("between-sorted.txt").readlines
out = File.open("centralities.txt", 'w')

i = 0
for line in deg
  line.chomp!
  id = line.split(" ")[0]
  out << "#{id} #{line.split(" ")[1]} #{btw[i].split(" ")[1]} #{evc[i].split(" ")[1]}\n"
  i += 1
end
