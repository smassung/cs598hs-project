# creates a giant matrix for R to do calculations on

require 'json'

puts "Reading in users..."
users = {}
File.open("../data/yelp_academic_dataset_user.json", 'r').each do |line|
  user = JSON.parse line
  users[user["user_id"]] = user
end

# these are just output files from centrality measures sorted by id
evc = File.open("eigen-sorted.txt", 'r').readlines
deg = File.open("degree-sorted.txt", 'r').readlines
btw = File.open("between-sorted.txt", 'r').readlines
out = File.open("yelp-frame.dat", 'w')

puts "Creating data frame..."
i = 0
out << "degree betweenness eigenvector friends fans useful reviews\n"
for line in deg
  line.chomp!
  id = line.split(" ")[0]
  elite = 0
  elite = 1 if users[id]["elite"].size > 0
  out << "#{line.split(" ")[1]} #{btw[i].split(" ")[1]} #{evc[i].split(" ")[1]} "
  friends = users[id]["friends"].size
  fans = users[id]["fans"]
  useful = users[id]["votes"]["useful"]
  rcount = users[id]["review_count"]
  out << "#{friends} #{fans} #{useful} #{rcount}\n"
  i += 1
end
