import json
from datetime import datetime
from matplotlib import pyplot

def main():
    """
    Scatter plot showing how the number of 'useful' counts vary
    with the word count

    """

    file_name = "data/yelp_academic_dataset_review.json"
    
    # duration graph
    useful_list = []
    duration = []

    # word count graph
    word_count = []


    latest = datetime.strptime("2014-07-16", "%Y-%m-%d")

    count = 0

    print("parsing data...")
    with open(file_name) as f:
        for line in f:
            #count +=1
            #if count>100000:
            #    break

            data = json.loads(line)
            
            useful = data["votes"]["useful"]
            useful_list.append(useful)

            # review date
            date = data["date"]
            date = datetime.strptime(date , "%Y-%m-%d")
            duration.append((latest-date).days)
            #print(latest, date, (latest-date).days, data["votes"]["useful"])

            # word count
            word_count.append(len(data["text"].split(' ')))


    #print("plotting useful vs duration graph...")
    #pyplot.scatter(duration, useful_list)
    #pyplot.xlim([0, max(duration)])
    #pyplot.ylim([0, max(useful_list)])
    #pyplot.title("useful vs duration")
    #pyplot.show()

    print("plotting useful vs word count graph...")
    pyplot.scatter(duration, useful_list)
    pyplot.xlim([0, max(word_count)])
    pyplot.ylim([0, max(useful_list)])
    pyplot.title("useful vs word count")
    pyplot.show()

if __name__ == "__main__":
    main()
