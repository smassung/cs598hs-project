# Read short list of user IDs sorted by some centrality measure. Display user
# info of each user to see what the centrality measure captures.

require 'json'

if ARGV.size != 1
  puts "Needs filename of user ids as parameter"
  exit
end

puts "Reading in users..."
users = {}
File.open("data/yelp_academic_dataset_user.json", 'r').each do |line|
  user = JSON.parse line
  users[user["user_id"]] = user
end

puts "#{' '*22}\tName\tReviews\tUseful\tFriends\tFans\tElite"
File.open(ARGV[0], 'r').each do |line|
  id = line.chomp
  user = users[id]
  puts "#{id}\t#{user["name"]}\t#{user["review_count"]}\t#{user["votes"]["useful"]}\t#{user["friends"].size}\t#{user["fans"]}\t#{user["elite"]}"
end
