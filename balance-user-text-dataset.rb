num_elite = 20045

out = File.open("yelp-user-text-balanced.dat", 'w')
outlabels = File.open("yelp-user-text-balanced.dat.labels", 'w')
outnames = File.open("yelp-user-text-balanced.dat.names", 'w')
outmeta = File.open("yelp-user-text-balanced.dat.meta", 'w')

inlabels = File.open("yelp-user-text.dat.labels").read.split("\n")
innames = File.open("yelp-user-text.dat.names").read.split("\n")
inmeta = File.open("yelp-user-text.dat.meta").read.split("\n")

normal = 0
File.open("yelp-user-text.dat").each_with_index do |line, i|
  elite = (inlabels[i] == "1")
  normal += 1 if not elite
  next if (not elite) and (normal > num_elite)
  out << line
  outlabels << "#{inlabels[i]}\n"
  outnames << "#{innames[i]}\n"
  outmeta << "#{inmeta[i]}\n"
end
