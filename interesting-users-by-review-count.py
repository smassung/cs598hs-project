import json
from datetime import datetime
from matplotlib import pyplot, dates
from mpl_toolkits.mplot3d import Axes3D
import pylab
from collections import defaultdict
import numpy as np
from datetime import datetime, date

def main():
    print("parsing data...")

    # user data
    file_name = "data/yelp_academic_dataset_user.json"

    # dictionary of
    # key: user_id
    # value: user object
    normal_users_dict = {}    # to make checking easier
    elite_users_dict = {}     # to make checking easier

    with open(file_name) as f:
        for line in f:
            data = json.loads(line)

            if len(data['elite']) > 0:
                elite_users_dict[data['user_id']] = data
            else:
                normal_users_dict[data['user_id']] = data


    # review data
    file_name = "data/yelp_academic_dataset_review.json"

    business_to_reviews = defaultdict(list)


    with open(file_name) as f:
        for line in f:
            review = json.loads(line)
            user_id = review['user_id']
            business_id = review['business_id']

            if user_id in elite_users_dict:
                user_type = 'elite'
            else:
                user_type = 'normal'

            review['user_type'] = user_type
            review['date'] = datetime.strptime(review['date'], "%Y-%m-%d")

            business_to_reviews[business_id].append(review)


    # find top k businesses
    business_and_reviewCount = []

    # store in list of tuples
    for id, reviews in business_to_reviews.iteritems():
        business_and_reviewCount.append((id, len(reviews)))
   
    # find top k businesses
    k = 2
    top_k_temp = sorted(business_and_reviewCount, key=lambda x: x[1], reverse=True)[:k] 
    # min_review_count = min(top_k_temp, key = lambda t: t[1])[1]

    # print("min review count for top " + str(k) + " businesses is " + str(min_review_count))

    # covert top k businesses back to dictionary with key=businessID and value=list of review objects
    top_k = {}

    for business_id, reviewCount in top_k_temp:
        top_k[business_id] = sorted(business_to_reviews[business_id], key=lambda review: (review['date']))


    # FOR PLOTTING
    # loop through the reviews of each top business
    topK = []
    review_th = []
    pElite_list = []

    n = 10
    kth = 1
    print("preparing graph...")
    for business_id, reviews in top_k.iteritems():

        reviews = sorted(reviews, key=lambda review: (review['date'])) # sort by date

        # first n reviews
        reviews = reviews[:n]

        elite_count = 0

        # pElite_list = []

        for i in range(0, len(reviews)):
            review = reviews[i]
            if review['user_type'] == 'elite':
                elite_count += 1


            pElite = float(elite_count)/(i+1)

            topK.append(kth)
            pElite_list.append(pElite)
            review_th.append(i+1)
    

  
        # pyplot.scatter(range(1,len(reviews)+1), pElite_list)
        # pyplot.show()
        kth+=1

    fig = pylab.figure()
    ax = Axes3D(fig)
    ax.scatter(topK, review_th, pElite_list)

    ax.set_xlabel("top k businesses")
    ax.set_ylabel("review-th")
    ax.set_zlabel("fraction of elite reviews")

    pyplot.show()



if __name__ == "__main__":
    main()




