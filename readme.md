# What is a Yelp expert?

 - They have a high number of "useful" reviews in their area of expertise given
   how much time they've been posted
 - This can be used to evaluate results (for example use some metric like
   useful votes per time for
   [NDCG](http://en.wikipedia.org/wiki/Discounted_cumulative_gain) scoring)
 - Domains that a user can be an expert are the possible categories that
   businesses are in (e.g. Shopping, Home Services, Food, Burgers)
 - Challenge: some categories are subsets of other categories

# Features

### Text features: is a review useful?

 - Text features tell whether a single review is useful
 - Are expert reviews similar to other expert reviews?
 - How are expert reviews similar or different to the average review for a
   category?
 - An expert posts first in a group of similar reviews? (Other users copy the
   expert's review)

### Network features: is a user an expert?

 - An expert makes many useful posts
 - A user can't be an expert in too many domains; the distribution of expert
   domains per user should be sparse
 - They have written a decent amount of reviews in each domain they are an
   expert in
 - Are expert users similar to other expert users? (use random walks,
   path-based similarities, neighborhood based, etc)
 - Other interesting attributes: elite status, fans, yelping since and compliments

# Questions

 - Can you predict which users **are** experts given the current network?
 - Can you predict which users **will be** experts?
 - What is the opposite of an expert user? If "expertness" is on a scale from
   0 to 1, is 0 spam?
