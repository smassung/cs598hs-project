\section{Experiments}

We now run experiments to test whether each feature generation method is a
viable candidate to distinguish between elite and normal users. As mentioned
before, the number of elite users is much smaller than the number of total
users; about $8\%$ of all 252,898 users are elite. This presents us with a very
imbalanced class distribution. Since using the entire user base to classify
elite users has such a high baseline ($92\%$ accuracy), we also truncate the
dataset to a balanced class distribution with a total of 40,090 users, giving a
alternate baseline of $50\%$ accuracy. Both datasets are used for all future
experiments.

As described in section 3.1, we use the MeTA
toolkit\footnote{\url{http://meta-toolkit.github.io/meta/}} to do the text
tokenization, class balancing, and five-fold cross-validation with SVM. SVM is
implemented here as stochastic gradient descent with hinge loss.

\subsection{Text Features}

We represent users as a collection of all their review text. Based on the
previous experiments, we saw that it was possible to classify a single review
as being written by an elite or normal user. Now, we want to classify users
based on all their reviews as either an elite or normal user.
Figure~\ref{fig:text-features} shows the results of the text classification
task. Using the balanced dataset we achieve about $77\%$ accuracy, compared to
barely achieving the baseline accuracy in the full dataset.

Since the text features are so high dimensional, we performed some basic feature
selection by selecting the most frequent features from the dataset. Before
feature selection, we had an accuracy on the balanced dataset of about $70\%$.
Using the top 100, 250, and 500 features all resulted in a similar accuracy of
around $76\%$. We use the reduced feature set of 250 in our experimental
results in the rest of this paper.

\begin{figure}[t]
    \begin{center}
        \textbf{Confusion Matrix: Balanced Text Features}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.651 & 0.349\\
            \hline
            normal & 0.124 & 0.876 \\
        \end{tabular}

        Overall Accuracy: $76.7\%$, baseline $50\%$

        \vspace{.2in}
        \textbf{Confusion Matrix: Unbalanced Text Features}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.582 & 0.418 \\
            \hline
            normal & 0.039 & 0.961 \\
        \end{tabular}

        Overall accuracy: $91.8\%$, baseline $92\%$
    \end{center}
    \caption{Confusion matrices for normal \emph{vs} elite users on balanced and
    unbalanced datasets.}
    \label{fig:text-features}
\end{figure}

\subsection{Temporal Features}

The temporal features consist of features derived using changes in the average
number of votes per review posted, the sum of the ranks of reviews of an user as
well as the tips, and the distribution of reviews posted over the lifetime of a
user. Using these features, we obtained the results shown in
Figure~\ref{fig:temporal-features}.

\begin{figure}[t]
    \begin{center}
        \textbf{Confusion Matrix: Balanced Temporal Features}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.790 & 0.210 \\
            \hline
            normal & 0.320 & 0.680 \\
        \end{tabular}

        Overall Accuracy: $73.5\%$, baseline $50\%$

        \vspace{.2in}
        \textbf{Confusion Matrix: Unbalanced Temporal Features}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.267 & 0.733 \\
            \hline
            normal & 0.067 & 0.933 \\
        \end{tabular}

        Overall accuracy: $88\%$, baseline $92\%$
    \end{center}
    \caption{Confusion matrices for normal \emph{vs} elite users on balanced and
    unbalanced datasets.}
    \label{fig:temporal-features}
\end{figure}

\subsection{Graph Features}

\begin{figure}[t]
    \begin{center}
        \textbf{Confusion Matrix: Balanced Graph Features}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.842 & 0.158\\
            \hline
            normal & 0.251 & 0.749 \\
        \end{tabular}

        Overall Accuracy: $79.6\%$, baseline $50\%$

        \vspace{.2in}
        \textbf{Confusion Matrix: Unbalanced Graph Features}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.311& 0.689\\
            \hline
            normal & 0.075& 0.925\\
        \end{tabular}

        Overall accuracy: $87.6\%$, baseline $92\%$
    \end{center}
    \caption{Confusion matrices for normal \emph{vs} elite users on balanced and
    unbalanced datasets.}
    \label{fig:graph-features}
\end{figure}

Figure~\ref{fig:graph-features} shows the results using the centrality measures
from the social network. Although there are only three features,
Figure~\ref{fig:centralities} showed that there is potentially a correlation
between the elite status and high-valued centrality measures. The three graph
features alone were able to predict whether a user was elite using the balanced
dataset with almost $80\%$ accuracy. Again, results were lower compared to the
baseline when using the full user set.

\subsection{Metadata Features}

\begin{figure}[t]
    \begin{center}
        \textbf{Confusion Matrix: Balanced Metadata Features}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.959& 0.041\\
            \hline
            normal & 0.083& 0.917\\
        \end{tabular}

        Overall Accuracy: $93.8\%$, baseline $50\%$

        \vspace{.2in}
        \textbf{Confusion Matrix: Unbalanced Metadata Features}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.880 & 0.120 \\
            \hline
            normal & 0.097 & 0.903 \\
        \end{tabular}

        Overall accuracy: $90.1\%$, baseline $92\%$
    \end{center}
    \caption{Confusion matrices for normal \emph{vs} elite users on balanced and
    unbalanced datasets.}
    \label{fig:metadata-features}
\end{figure}

Using only the six metadata features from the original Yelp JSON file gave
surprisingly high accuracy at almost $94\%$ for the balanced classes. In fact,
the metadata features had the highest precision for both the elite and normal
classes. The unbalanced accuracy was near the baseline.

\subsection{Feature Combination and Discussion}

\begin{figure}[t]
    \begin{center}
        \textbf{Confusion Matrix: Balanced All Features}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.754& 0.256\\
            \hline
            normal & 0.111& 0.889\\
        \end{tabular}

        Overall Accuracy: $82.2\%$, baseline $50\%$

        \vspace{.2in}
        \textbf{Confusion Matrix: Unbalanced All Features}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.976& 0.024\\
            \hline
            normal & 0.731& 0.269\\
        \end{tabular}

        Overall accuracy: $92\%$, baseline $92\%$
    \end{center}
    \caption{Confusion matrices for normal \emph{vs} elite users on balanced and
    unbalanced datasets with all features present.}
    \label{fig:all-features}
\end{figure}

To combine features, we simply concatenated the feature vectors for all the
previous features and used the same splits and classifier as before.
Figure~\ref{fig:all-features} shows the breakdown of this classification
experiment. Additionally, we summarize all results by final accuracy in
Figure~\ref{fig:final-results}.

Unfortunately, it looks like the combined feature vectors did not significantly
improve the classification accuracy on the balanced dataset as expected.
Initially, we though that this might be due to overfitting, which is why we
reduced the number of text features from over 70,000 to 250. Using the 70,000
text features combined with the other feature types resulted in about $70\%$
accuracy; with the top 250 features, we achieved $82.2\%$ as shown in the
tables. For the unbalanced dataset, it seems that the results did improve to
reach the difficult baseline.

Using all combined features \emph{except} the text features resulted in $90.4\%$
accuracy, suggesting there is some sort of disagreement between ``predictive''
text features and all other predictive features. Thus, removing the text
features yielded a much higher result, approaching the accuracy of just the Yelp
metadata features.

\begin{figure}[t]
    \begin{center}
        \begin{tabular}{|l|lllll|}
            \hline
            & \textbf{Text} & \textbf{Temp.} & \textbf{Graph} &
            \textbf{Meta} & \textbf{All} \\
            \hline
            Balanced   & .767 & .735 & .796 & .938 & .822$^*$ \\
            Unbalanced & .918 & .880 & .876 & .901 & .920 \\
            \hline
        \end{tabular}
    \end{center}
    \caption{Final results summary for all features and feature combinations on
    balanced and unbalanced data. $^*$Excluding just the text features resulted
in $90.4\%$ accuracy.}
    \label{fig:final-results}
\end{figure}

Since we dealt with some overfitting issues, we made sure that the classifier
used regularization. Regularization ensures that weights for specific features
do not become too high if it seems that they are incredibly predictive of the
class label. Fortunately (or unfortunately), the classifier we used does employ
regularization, so there is nothing we could further do to attempt to increase
the performance.
