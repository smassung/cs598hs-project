\section{Text Analysis}

The text analysis examines the reviews written by each user in order to extract
features from the unstructured text content. Common text processing techniques
such as indexing, categorization, and language modeling are explored in the next
sections.

\subsection{Datasets}

First, we preprocessed the text by lowercasing, removing stop words, and
performing stemming with the Porter2 English stemmer. Text was tokenized as
unigram bag-of-words by the MeTA
toolkit\footnote{\url{http://meta-toolkit.github.io/meta/}}.

We created three datasets that are used in this section:

\begin{itemize}
    \item \textbf{All}: contains all the elite reviews (reviews written by an
    elite user) and an equal number of normal reviews
    \item \textbf{Elite}: contains only reviews written by elite users
    \item \textbf{Normal}: contains only reviews written by normal users
\end{itemize}

Elite and Normal together make up All; this is to ensure the analyses run on
these corpora have a balanced class distribution. Overall, there were 1,125,458
reviews, consisting of 329,624 elite reviews and 795,834 normal reviews. Thus,
the number of normal reviews was randomized and truncated to 329,624.

The number of elite users is far fewer than the ratio of written reviews may
suggest; this is because elite users write many more reviews on average than
normal users. A summary of the three datasets is found in
Fig~\ref{fig:text-datasets}.

\begin{figure}[t]
    \begin{center}
    \begin{tabular}{lrrrrr}
    \hline
    \hline
    \textbf{Dataset} & \textbf{Docs} & \textbf{Len$_{avg}$} & \textbf{$|V|$} &
    \textbf{Raw} & \textbf{Index} \\
    \hline
    All & 659,248 & 81.8 & 164,311 & 480 & 81 \\
    Elite & 329,624 & 98.8 & 125,137 & 290 & 46 \\
    Normal & 329,624 & 64.9 & 95,428 & 190 & 37 \\
    \hline
    \hline
    \end{tabular}
    \end{center}
    \caption{Comparison of the three text datasets of Yelp reviews in terms of
    corpus size, average document length (in words), vocabulary size, raw data
    size, and indexed data size (both in MB).}
    \label{fig:text-datasets}
\end{figure}

\subsection{Classification}

We tested how easy it is to distinguish between an elite review and a non-elite
(normal) review by a simple supervised classification task. We used the dataset
All described in the previous section along with each review's true label to
train an SVM classifier. Evaluation was performed with five-fold cross
validation and had a baseline of 50\% accuracy. Results of this categorization
experiment are displayed in Fig~\ref{fig:classification}.

\begin{figure}[t]
    \begin{center}
        \textbf{Confusion Matrix}
        \begin{tabular}{c|c|c}
            & classified as elite & classfied as normal \\
            \hline
            elite & 0.665 & 0.335 \\
            \hline
            normal & 0.252 & 0.748 \\
        \end{tabular}

        \vspace{.2in}
        \begin{tabular}{llll}
            \hline
            \hline
            \textbf{Class} & \textbf{F1 Score} & \textbf{Precision} & \textbf{Recall} \\
            \hline
            Elite & 0.694 & 0.665 & 0.725 \\
            Normal & 0.718 & 0.748 & 0.691 \\
            \hline
            Total & 0.706 & 0.706 & 0.708 \\
            \hline
            \hline
        \end{tabular}
    \end{center}
    \caption{Confusion matrix and classification accuracy on normal \emph{vs}
    elite reviews.}
    \label{fig:classification}
\end{figure}

The confusion matrix tells us that it was slightly easier to classify normal
reviews, though the overall accuracy was acceptable at just over 70\%.
Precision and recall highs had opposite maximums for normal and elite, though
overall the $F_1$ scores were similar. Recall the $F_1$ score is a harmonic mean
of precision $P$ and recall $R$:

$$F_1 = \frac{2PR}{P+R}$$

Since this is just a baseline classifier, we expect it is possible to achieve
higher accuracy using more advanced features such as $n$-grams of words or
grammatical features like part-of-speech tags or parse tree productions.
However, this initial experiment is to determine whether elite and non-elite
reviews can be separated based on text alone, with no regard to the author or
context. Since the accuracy on this default model is $70\%$ is seems that text
will make a useful subset of overall features to predict expertise.

Furthermore, remember that this classification experiment is not whether a
\emph{user} is elite or not, but rather whether a \emph{review} has been written
by an elite user; it would be very straightforward to extend this problem to
classify users instead, where each user is a combination of all reviews that he
or she writes. In fact, this is what we do in section 7, where we are concerned
with identifying elite users.

\subsection{Language Model}

We now turn to the next text analysis method: unigram language models. A
language model is simply a distribution of words given some context. In our
example, we will define three language models---each based on a corpus described
in section 3.1.

The background language model (or ``collection'' language model) simply
represents the All corpus. We define a smoothed collection language model
$p_C(w)$ as

$$p_C(w) = \frac{count(w,C)+1}{|C|+|V|}$$

This creates a distribution $p_C(\cdot)$ over each word $w\in V$. Here, $C$ is
the corpus, and $V$ is the vocabulary (each unique word in $C$), so $|C|$ is the
total number of words in the corpus and $|V|$ is the number of unique terms.

The collection language model essentially shows the probability of a word
occurring in the entire corpus. Thus, we can sort the outcomes in the
distribution by their assigned probabilities to get the most frequent words.
Unsurprisingly, these words are the common stop words with no real content
information. However, we will use this background model to filter out words
specific to elite or normal users.

We now define another unigram language model to represent the probability of
seeing a word $w$ in a corpus $\theta \in \{elite, normal\}$. We create a
normalized language model score per word using the smoothed background model
defined previously:

$$score(w,\theta) = \frac{\frac{count(w,\theta)}{|\theta|}}{p_C(w)} =
\frac{count(w,\theta)}{p_C(w)\cdot |\theta|}$$

The goal of the language model score is to find unigram tokens that are very
indicative of their respective categories; using a language model this way can
be seen as a form of feature selection. Fig~\ref{fig:lm} shows a comparison of
the top twenty words from each of the three methods.

\begin{figure}[t]
    \begin{center}
        \begin{tabular}{ccc}
            \hline
            \hline
            \textbf{Background} & \textbf{Normal} & \textbf{Elite} \\
            \hline
             the    & gorsek    &    uuu                 \\
             and    & forks)    &    aloha!!!               \\
             a      & yu-go     &    **recommendations**    \\
             i      & sabroso   &    meter:            \\
             to     & (\****    &    **summary**       \\
             was    & eloff     &    carin             \\
             of     & -/+       &    no1dp             \\
             is     & jeph      &    (lyrics           \\
             for    & deirdra   &    friends!!!!!      \\
             it     & ruffin'   &    **ordered**       \\
             in     & josefa    &    8/20/2011         \\
             that   & ubox      &    rickie            \\
             my     & waite     &    kuge              \\
             with   & again!!   &    ;]]]              \\
             but    & optionz   &    \#365             \\
             this   & ecig      &    g                 \\
             you    & nulook    &    *price            \\
             we     & gtr       &    visits):          \\
             they   & shiba     &    r\_               \\
             on     & kenta     &    ik                \\
            \hline
            \hline
        \end{tabular}
    \end{center}
    \caption{Top 20 tokens from each of the three language models.}
    \label{fig:lm}
\end{figure}

These default language models did not reveal very clear differences in
word usage between the two categories, despite the elite users using a larger
vocabulary as shown in Fig~\ref{fig:text-datasets}. The singular finding was
that the elite language model shows that its users are more likely to segment
their reviews into different sections, discussing different aspects of the
business. For example, recommendations, summary, ordered, or price.

Also, it may appear that there are a good deal of nonsense words in the top
words from each language model. However, upon closer inspection, these words are
actually valid given some domain knowledge of the Yelp dataset. For example, the
top word ``gorsek'' in the normal language model is the last name of a normal
user that always signs his posts. Similarly, ``sabroso'' is a Spanish word
meaning delicious that a particular user likes to say in his posts. Similar
arguments can be made for other words in the normal language model. In the elite
model, ``uuu'' was originally ``\textbackslash uuu/'', an emoticon that an elite
user is fond of. ``No1DP'' is a Yelp username that is often referred to by a few
other elite users in their review text.

Work on supervised and unsupervised review aspect segmentation has been done
before~\cite{lara,laram}, and it may be applicable in our case since there are
clear boundaries in aspect mentions. Another approach would be to add a boolean
feature \texttt{has\_aspects} that detects whether a review is segmented in the
style popular among elite users.

\subsection{Typographical Features}

Based partly on the experiments performed in section 3.3, we now define
typographical features of the review text. We call a feature a `typographical'
feature if it is a trait that can't be detected by a unigram words tokenizer
and is indicative of the style of review writing.

We use the following six style or typographical features:

\begin{itemize}
    \item \textbf{Average review length}. We calculate review length as the
        number of whitespace-delimited tokens in a review. Average review length
        is simply the average of this count across all of a user's reviews.
    \item \textbf{Average review sentiment}. We used sentiment valence
        scores~\cite{sentiment} to calculate the sentiment of an entire review.
        The sentiment valence score is $<0$ if the overall sentiment is negative
        and $>0$ if the overall sentiment is positive.
    \item \textbf{Paragraph rate}. Based on the language model analysis, we
        included a feature to detect whether paragraph segmentation was used in
        a review. We simply count the rate of multiple newline characters per
        review per user.
    \item \textbf{List rate}. Again, based on the language model analysis, we
        add this feature to detect whether a bulleted list is included in the
        review. We defined a list as the beginning of a line followed by `*' or
        `-' before alpha characters.
    \item \textbf{All caps}. The rate of words in all capital letters. We
        suspect very high rates of capital letters will indicate spam or useless
        reviews.
    \item \textbf{Bad punctuation}. Again, this feature is to detect less
        serious reviews in an attempt to find spam. A basic example of bad
        punctuation is not starting a new sentence with a capital letter.
\end{itemize}

Although the number of features here is low, we hope that the added meaning
behind each one is more informative than a single unigram words feature.
