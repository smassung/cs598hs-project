\section{Introduction}

Expert finding seeks to locate users in a particular domain that have more
qualifications or knowledge (expertise) than the average user. Usually, the
number of experts is very low compared to the overall population, making this a
challenging problem. Expert finding is especially important in medical, legal,
and even governmental situations. In our work, we focus on the Yelp academic
dataset\cite{yelp-dataset} since it has many rich features that are unavailable
in other domains. In particular, we have full review content, timestamps, the
friend graph, and user metadata---this allows us to use techniques from text
mining, time series analysis, social network analysis, and classical machine
learning.

From a user's perspective, it's important to find an expert reviewer to give a
fair or useful review of a business that may be a future destination. From a
business's perspective, expert reviewers should be great summarizers and able to
explain exactly how to improve their store or restaurant. In both cases, it's
much more efficient to find the opinion of an expert reviewer than sift through
hundreds of thousands of potentially useless or spam reviews.

Yelp is a crowd-sourced business review site as well as a social network,
consisting of several objects: users, reviews, and businesses. Users write text
reviews accompanied by a star rating for businesses they visit. Users also have
bidirectional friendships as well as one-directional fans. We consider the
social network to consist of the bidirectional friendships since each user
consents to the friendship of the other user. Additionally, popular users are
much less likely to know their individual fans making this connection much
weaker. Each review object is annotated with a time stamp, so we are able to
investigate trends temporally.

The purpose of this work is to investigate and analyze the Yelp dataset and find
potentially interesting patterns that we can exploit in our future
expert-finding system. The key question we hope to answer is:

\begin{center}
\emph{Given a network of Yelp users, who is an elite user?}
\end{center}

%\emph{Given a current time period, who will become an elite user in the next
%time period?}

To answer the above question, we have to first address the following:

\begin{enumerate}
\item How does the text in expert reviews differ from text in normal reviews?
\item How does the average number of votes per review for a user change over
    time?
\item Are elite users the first to review a new business?
\item Does the social network structure suggest whether a user is an elite
    user?
\item Does user metadata available from Yelp have any indication about a user's
    status?
\end{enumerate}

The structure of this paper is as follows: in section 2, we discuss related
work. In sections 3, 4, 5, and 6, we discuss the four different dimensions of
the Yelp dataset. For the first three feature types, we use text analysis,
temporal analysis, and social network analysis respectively. The user metadata
is already in a quantized format, so we simply overview the fields available.
Section 7 details running experiments on the proposed features on balanced
(number of experts is equal to the number of normal users) and unbalanced
(number of experts is much less) data. Finally, we end with conclusions and
future work in section 8.
