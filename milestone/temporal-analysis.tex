\section{Temporal Analysis}
In this section, we look at how features change temporally by making use of the
time stamp in reviews as well as tips. This allows to us to analyze the activity
of a user over time as well as how the average number of votes the user has
received changes with each review posted.

\subsection{Average Votes-per-review Over Time}

For the average number of votes-per-review varies with each review posted by an user. 
To gather this data, we grouped the reviews in the Yelp dataset by users and ordered
the reviews by the date each was posted. 

The goal was to try to predict whether an user is an ``Elite'' or ``Normal'' user
using the votes-per-review vs review number plot. The motivation for this was that
after processing the data, we found out that the number of votes on average was significantly
greater for elite users compared to normal users as show in Fig~\ref{fig:elite-vs-normal-votes}.
Thus, we decided to find out whether any trend exists on how the average number of votes
grow with each review posted by users from both categories. We hypothesized that elite
users should have an increasing average number of votes over time.

\begin{figure}[t]
    \begin{center}
        \textbf{Elite vs Normal users Statistics}
        \begin{tabular}{c|c|c|c}
            & useful votes & funny votes & cool votes \\
            \hline
            elite users & 616 & 361 & 415 \\
            \hline
            normal users & 20 & 7 & 7 \\
        \end{tabular}
    \end{center}
    \caption{Average number of votes per category for elite and normal users.}
    \label{fig:elite-vs-normal-votes}
\end{figure}


On the $y$-axis, we have $\upsilon_{i}$ which is the votes-per-review after a
user posts his $i^{th}$ review. This is defined as the sum of the number of
``useful'' votes, ``cool'' votes and ``funny'' votes divided by the number of
reviews by the user up to that point in time. On the $x$-axis, we will have the review count.

Using the Yelp dataset, we plotted a scatter plot for each user. Visual inspection
of graphs did not show any obvious trends in how the average number of likes per review
varied with each review being posted by the user.

We then proceeded to perform a logistic regression using the following variables:

$$ P_{increase}=\frac{count(increases)}{count(reviews)} $$

$$ \mu = \frac{\sum_{i=0}^{count(reviews)}\upsilon_{i}}{count(reviews)} $$

where $count(increases)$ is the number of times the average votes-per-review increased (i.e.
$\upsilon_{i+1} > \upsilon_{i}$) after a user posts a review and $count(reviews)$ is the
number of reviews the user has made.

Both the training and testing sets consists of only users with at least one review. For each
user, we calculated the variables $P_{increase}$ and $\mu$. The training and testing
data are shown in Fig~\ref{fig:lr}. 10\% of users with at least one review became part
of the training data and the remaining 90\% were used to test.

\begin{figure}[t]
    \begin{center}
        \textbf{Logistic Regression Summary}
        \begin{tabular}{c|c|c}
            & elite users & normal users \\
            \hline
            training & 2005 & 2005 \\
            \hline
            testing & 18040 & 18040 \\
        \end{tabular}
    \end{center}
    \caption{Summary of training and testing data for logistic regression.}
    \label{fig:lr}
\end{figure}

There was an accuracy of 0.69 on the testing set. The results are shown in Fig~\ref{fig:lr-results}.

\begin{figure}[t]
    \begin{center}
        \textbf{Confusion Matrix}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.64 & 0.36 \\
            \hline
            normal & 0.26 & 0.74 \\
        \end{tabular}
    \end{center}
    \caption{Summary of results for logistic regression.}
    \label{fig:lr-results}
\end{figure}

Given the overall accuracy of our model is relatively high at 0.69, we can hypothesize
that $P_{increase}$ is higher for elite users compared to normal users. This means that
each review that a elite user posts tends to be a ``quality'' review that receives enough
votes to increase the running average of votes-per-review for this user. The second hypothesis
is that the mean of the running average votes-per-review for elite users is higher than
that of normal users. This is supported by data shown in Fig~\ref{fig:elite-vs-normal-votes}
where the average votes for elite users are higher than normal users.

\subsection{User Review Rank}
For the second part of our temporal analysis, we look at the rank of each review a user has
posted. Using 0-index, if a review has rank $r$ for business $b$, the review was the $r_{th}$ review written
for business b.

Our hypothesis was that an elite user should be one of the first few users who write a review for a restaurant
because elite users are more likely to find new restaurants to review. Also, based on the dataset, elite
users write approximately 230 more reviews on average than normal users, thus it is more likely that elite users
will be one of the first users to review a business. Over time, since there are more normal users, the ratio of
elite to normal users will decrease as more normal users write reviews.

To verify this, we calculated the percentage of elite reviews for each rank across the top 10,000 businesses, whereby
the top business is defined as the business with the most reviews. The number of ranks we look at will be
the minimum number of reviews of a single business among the top 10,000 businesses. The plot is
shown in Fig~\ref{fig:review_rank_probability}.

\begin{figure}[t]
    \begin{center}
    \includegraphics[width=.5\textwidth]{review_rank_probability}
    \end{center}
\caption{Plot of the probability of being an elite user for reviews at rank r.}
\label{fig:review_rank_probability}
\end{figure}

Given that the dataset consists of approximately 10\% elite users, the plot shows us that it is more likely
for an elite user to be among the first few reviewers of a business.

We calculated a score for each user which is a function of the rank of each
review of the user and we included this as a feature in the SVM. For each review
of a user, we find the total number of reviews the business that this review
belongs to has. We take the total review count of this business and subtract the
rank of the review from it. We then sum this value for each review to assign a
score to the user. Based on our hypothesis, since elite users will more likely
have a lower rank for each review than normal users, the score for elite users
should therefore be higher than normal users.

The score for a review is defined as follows:

$$ score = \sum_{rev} (review\_count(business\_of(rev))-rank(rev))$$

We subtract the rank from the total review count so that based on our hypothesis, elite users will 
end up having a higher score.

\subsection{User Tip Rank}
A tip is a short chunk of text that a user can submit to a restaurant via any Yelp mobile application.
Using 0-index, if a tip has rank $r$ for business $b$, the tip was the $r_{th}$ tip written for business b.
Similar to the review rank, we we hypothesized that an elite user should be one of the first few tippers (person who
gives a tip) of a restaurant. We plotted the same graph which shows the percentage of elite tips for each rank
across the top 10,000 businesses, whereby the top business is defined as the business with the most tips. The
plot is shown in Fig~\ref{fig:tip_rank_probability}.

\begin{figure}[t]
    \begin{center}
    \includegraphics[width=.5\textwidth]{tip_rank_probability}
    \end{center}
\caption{Plot of the probability of being an elite user for tips at rank r.}
\label{fig:tip_rank_probability}
\end{figure}

Given that the dataset consists of approximately 10\% elite users, the plot shows us that it is more likely
for an elite user to be among the first few tippers of a business. Furthermore, for this specific dataset,
elite users only make up approximately 25\% of the total number of tips, yet for the top 10,000 businesses,
they make up more than 25\% of the tips for almost all the ranks shown in Fig~\ref{fig:tip_rank_probability}.

We then calculated a score for each user based on the rank of each tip of the user and
we included this as a feature in the SVM. The score is defined as follows:

$$ score =  {\sum_{tip}^{} (tip\_count(business\_of(review))-rank(tip))}  $$ 

(The equation for this score follows the same reasoning as the user review rank section)

\subsection{Review Activity Window}
In this section, we look at the distribution of a user's activity over time. The window we look at is between
the user's join date and end date, defined as the last date of any review posted in the entire dataset. For each user, we will find
the interval in days between each review, including the join date and end date.
For example if the user has two reviews on
date1 and date2, where date2 is after date1, the interval durations will be: date1-joinDate, date2-date1 and endDate-date2. So for 
$n$ number of reviews, we will get $n+1$ intervals. Based on the list of intervals, we will calculate a score. For this feature,
we hypothesize that the lower the score, the more likely the user is an elite user.

The score is defined as:

$$ score =  \frac{var(intervals)+avg(intervals)}{days\_on\_yelp}  $$ 

Where var(intervals) is the variance of all the interval values, avg(intervals) is the average and days\_on\_yelp is the number
of days a user has been on Yelp.

For the variance, the hypothesis is that for elite users, the variance will tend to be low as we hypothesize 
that elite users should post regularly. For normal users, the variance will be high possibly due to irregular posting 
and long periods of inactivity between posts.

We also look at the average value of the intervals. This is because if we were to only look at variance, a user who writes a review
every two days will get the same variance (zero) as a user who writes a review every day. As such the average of the intervals will account
for this by increasing the score

Finally, we divide the score by the number of days the user has been on Yelp. This is to account for situations where a user makes a post
every week but has only been on Yelp for three weeks, versus a user who makes a post every week as well but has been on Yelp for 
a year. The user who has been on Yelp for a year will then get a lower value for this score (elite user).
