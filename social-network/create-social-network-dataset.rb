require 'json'

# first, determine the number of reviews written by each user
puts "Counting reviews..."

num_reviews = {}
File.open("../data/yelp_academic_dataset_user.json", 'r').each do |line|
  user = JSON.parse line
  num_reviews[user["user_id"]] = user["review_count"]
end

# then actually create the dataset
puts "Creating network dataset..."

min_reviews = 1500
out = File.open("yelp-friends.dat", 'w')
File.open("../data/yelp_academic_dataset_user.json", 'r').each do |line|
  user = JSON.parse line
  next if user["review_count"] < min_reviews
  for friend in user["friends"]
    next if num_reviews[friend] < min_reviews
    out << "#{user["user_id"]} #{friend}\n"
  end
end
