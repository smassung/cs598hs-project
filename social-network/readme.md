`create-social-network-dataset.rb` creates the graph input file readable by
[MeTA](http://meta-toolkit.github.io/meta/) with `social-network.cpp`.

The C++ code outputs a JSON file readable by the d3 Javascript to make a visual
of the friends network.

It seems like the entire network is too large to visualize in the browser, so I
made it only select nodes that have above a certain number of reviews. Still
not sure how useful the visualization is.
