/**
 * @file yelp-social.js
 * Based on http://mbostock.github.com/d3/ex/force.html
 */

var width  = 1000;
var height = 1000;
var radius = 9;
var linkDistance = 30;
var maxEdgeWidth = 10;
var charge = -600;
var color = d3.scale.category20();

var force = d3.layout.force()
  .charge(charge)
  .linkDistance(linkDistance)
  .size([width, height]);

var svg = d3.select("#chart").append("svg")
  .attr("width", width)
  .attr("height", height);

var showNodeDetail = function() {
  d3.select(this)
    .style("stroke", "black")
    .style("stroke-width", 1.5)
    .attr("stroke-opacity", 0.7);
};

var hideNodeDetail = function() {
  d3.select(this)
    .style("stroke", "none");
};

d3.json("yelp-social.json", function(json) {

  console.log(json.nodes.length + " nodes");
  console.log(json.links.length + " edges");

  force
    .nodes(json.nodes)
    .links(json.links)
    .start();

  var link = svg.selectAll("line.link")
    .data(json.links)
    .enter().append("line")
    .attr("class", "link")

  var node = svg.selectAll("circle.node")
    .data(json.nodes, function(d) { return d.id; })
    .enter().append("circle")
    .attr("class", "node")
    .on("mouseover", showNodeDetail)
    .on("mouseout", hideNodeDetail)
    .call(force.drag);

  node
    .append("title")
    .text(function(d) { return d.name; });

  force.on("tick", function() {
    node
  //  .each(function(d) { if(d.visible == false) d3.select(this).remove(); })
      .attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; });
    link
      .attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; });
  });
});
