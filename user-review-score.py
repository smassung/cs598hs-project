import json
from datetime import datetime
from matplotlib import pyplot, dates
from mpl_toolkits.mplot3d import Axes3D
import pylab
from collections import defaultdict
import numpy as np
from datetime import datetime, date
from sklearn import linear_model

# calculates the sum of the rank of each review by a user
def main():
    print("parsing data...")

    # user data
    file_name = "data/yelp_academic_dataset_user.json"

    # dictionary of
    # key: user_id
    # value: user object
    normal_users_dict = {}    # to make checking easier
    elite_users_dict = {}     # to make checking easier
    all_users = {}

    with open(file_name) as f:
        for line in f:
            data = json.loads(line)
            all_users[data['user_id']] = data
            if len(data['elite']) > 0:
                elite_users_dict[data['user_id']] = data
            else:
                normal_users_dict[data['user_id']] = data


    # review data
    file_name = "data/yelp_academic_dataset_review.json"

    business_to_reviews = defaultdict(list)
    user_to_reviews = defaultdict(list)
    total_review_count = 0

    with open(file_name) as f:
        for line in f:
            total_review_count+=1

            review = json.loads(line)
            user_id = review['user_id']
            business_id = review['business_id']

            if user_id in elite_users_dict:
                user_type = 'elite'
            else:
                user_type = 'normal'

            review['user_type'] = user_type
            review['date'] = datetime.strptime(review['date'], "%Y-%m-%d")

            business_to_reviews[business_id].append(review)
            user_to_reviews[user_id].append(review)


    print("calculating rank score...")

    review_id_to_rank = {}
    # find rank of each review
    for business_id, reviews in business_to_reviews.iteritems():

        max_score = len(reviews)
        for i in range(0, len(reviews)):
            review = reviews[i]
            review_id = review['review_id']

            review_id_to_rank[review_id] = max_score-i

    # find total rank score of each user
    f = open('user-review-score.txt','w')
    user_to_score = {}
    user_score = []
    for user, reviews in user_to_reviews.iteritems():
        score = 0
        for review in reviews:
            review_id = review['review_id']
            score += review_id_to_rank[review_id]

        f.write(' '.join([user, str(score), '\n']))

        user_to_score[user] = score

        if len(all_users[user]['elite']) > 0:
            user_score.append(('elite', score))
            print("elite ", score)
        else:
            user_score.append(('normal', score))
            print("normal ", score)
    f.close()


    print sorted(user_score, key= lambda k:k[1], reverse=True)[:100]

    return

if __name__ == "__main__":
    main()