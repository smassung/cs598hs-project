import json
from datetime import datetime
from matplotlib import pyplot
from collections import defaultdict
import numpy as np

from sklearn import linear_model

def createVector(reviews):
    funny_count = 0
    useful_count = 0
    cool_count = 0
    vote_count = 0

    reviewCount = 0
    increaseCount = 0

    prev_avg = 0

    votes_per_review = []

    best_consec = 0     # longest increasing sequence
    consec = 0

    for i in range(len(reviews)):
        review = reviews[i]

        # useful per review
        funny_count += review['votes']['funny']

        # useful per review
        useful_count += review['votes']['useful']

        # useful per review
        cool_count += review['votes']['cool']

        # aggregate of all 3 above
        vote_count = funny_count + useful_count + cool_count

        avg = float(vote_count)/(i+1)
        votes_per_review.append(avg)

        reviewCount += 1

        if avg >= prev_avg:
            consec += 1
            increaseCount += 1
        else:
            # not consecutive increases
            if consec > best_consec:
                best_consec = consec
            consec = 0

        prev_avg = avg

    if consec > best_consec:
        best_consec = consec


    # grad, c = np.polyfit(range(reviewCount), votes_per_review, 1)

    pIncrease = float(increaseCount)/(reviewCount)
    pIncrease = int(pIncrease*100)

    pBestConsec = float(best_consec)/reviewCount

    vector = [pIncrease, sum(votes_per_review)/reviewCount]

    return vector

def main():
    print("parsing data...")

    # user data
    file_name = "../data/yelp_academic_dataset_user.json"

    # dictionary of
    # key: user_id
    # value: user object
    normal_users = []
    normal_users_dict = {}    # to make checking easier
    elite_users = []
    elite_users_dict = {}     # to make checking easier

    with open(file_name) as f:
        for line in f:
            data = json.loads(line)

            if len(data['elite']) > 0:
                elite_users.append(data)
                elite_users_dict[data['user_id']] = data
            else:
                normal_users.append(data)
                normal_users_dict[data['user_id']] = data


    # review data
    file_name = "../data/yelp_academic_dataset_review.json"

    # dictionary of 
    # key: user_id
    # value: review list
    all_user_reviews = defaultdict(list)

    with open(file_name) as f:
        for line in f:
            review = json.loads(line)
            user_id = review['user_id']
            all_user_reviews[user_id].append(review)


    count = 0
    for user, reviews in all_user_reviews.iteritems():
        count += len(reviews)

    print("total number of reviews: " + str(count))
    print("total number of users " + str(len(elite_users_dict)+len(normal_users_dict)))
    print("elites users " + str(len(elite_users_dict)))
    print("normal users " + str(len(normal_users_dict)))
    print("")


    # create features output to file
    f = open('temporal_features.txt','w')
    for user in all_user_reviews:
        reviews = all_user_reviews[user]
        if len(reviews) > 0:
            reviews = sorted(reviews, key= lambda k:k['date'])  # sort in ascending date order
            vector = createVector(reviews)

            vector = [str(feature) for feature in vector]
            print(vector)
            f.write(' '.join([user] + vector + ['\n']))
    f.close()
    return
    # end output to file


    # train on 0.75 of each part of the data
    ratio = 0.10

    eliteTrain = len(elite_users_dict)*float(ratio)
    normalTrain = len(normal_users_dict)*float(ratio)
    normalTrain = eliteTrain

    eliteTest = len(elite_users) * float(1-ratio)
    normalTest = len(normal_users_dict)*float(1-ratio)
    normalTest = eliteTest

    eliteTrainCount = 0
    normalTrainCount = 0

    eliteTestCount = 0
    normalTestCount = 0

    train_user_reviews = {}
    test_user_reviews = {}

    for user in all_user_reviews:
        if user in elite_users_dict:
            if eliteTrainCount < eliteTrain:
                train_user_reviews[user] = all_user_reviews[user]
                eliteTrainCount += 1
            elif eliteTestCount < eliteTest :
                test_user_reviews[user] = all_user_reviews[user]
                eliteTestCount += 1
        else:
            if normalTrainCount < normalTrain:
                train_user_reviews[user] = all_user_reviews[user]
                normalTrainCount += 1
            elif normalTestCount < normalTest:
                test_user_reviews[user] = all_user_reviews[user]
                normalTestCount += 1


    print("training size: " + str(len(train_user_reviews)))
    print("elites training size: ", str(eliteTrainCount))
    print("normal training size: ", str(normalTrainCount))
    print("")
    print("testing size: " + str(len(test_user_reviews)))
    print("elites testing size: ", str(eliteTestCount))
    print("normal testing size: ", str(normalTestCount))
    print("")


    trainX = []
    trainY = []

    trainingCount = 0

    threshold = 0

    ##################################
    # build vectors for training set #
    ##################################
    for user in train_user_reviews:
        reviews = train_user_reviews[user]

        if len(reviews) > 0:
            trainingCount += 1
            reviews = sorted(reviews, key= lambda k:k['date'])  # sort in ascending date order

            vector = createVector(reviews)

            # train the model
            if user in elite_users_dict:
                # elite user
                trainY.append(1)
            else:
                # normal user
                trainY.append(0)

            trainX.append(vector)

    #############################
    # build logistic regression #
    #############################
    print("building logistic regression model using training set size: " + str(trainingCount))
    lr = linear_model.LogisticRegression()
    lr.fit(trainX, trainY)
    print("running tests on model...")

    #################
    # test on model #
    #################

    falsePos = 0
    falseNeg = 0
    correct = 0
    pos = 0
    neg = 0

    testCount = 0
    testElite = 0
    testNormal = 0

    testEliteCount = 0      # number of elites inside the test set
    testNormalCount = 0     # number of normals inside the test set

    # find the number of elite and normal users with more than threshold reviews
    # inside the testing set
    for user in test_user_reviews:
        if user in elite_users_dict:
            if len(test_user_reviews[user])>threshold:
                testEliteCount += 1
        else:
            if len(test_user_reviews[user])>threshold:
                testNormalCount += 1

    tests = min(testEliteCount, testNormalCount)
    print("number of tests for each of elite and normal users: " + str(tests))

    for user in test_user_reviews:
        reviews = test_user_reviews[user]
        if len(reviews) > threshold:

            if user in elite_users_dict:
                if testElite < tests:
                    testCount += 1
                    testElite += 1
                    pass
                else:
                    continue
            else:
                if testNormal < tests:
                    testCount  += 1
                    testNormal += 1
                    pass
                else:
                    continue

            reviews = sorted(reviews, key= lambda k:k['date'])  # sort in ascending date order

            vector = createVector(reviews)
            # test the model
            if lr.predict(vector) == 1:
                predicted = "elite"
            else:
                predicted = "normal"

            if user in elite_users_dict:
                truth = "elite"
            else:
                truth = "normal"


            if predicted==truth:
                if predicted == "elite":
                    pos += 1
                else:
                    neg += 1
                correct += 1
            else:
                if predicted == "elite":
                    falsePos += 1
                else:
                    falseNeg += 1

    print(pos, neg, falsePos, falseNeg)

    print("test set size : " + str(testCount))
    print("elite users test : " + str(testElite))
    print("normal users test : " + str(testNormal))
    print("")

    print("review count threshold: " + str(threshold))

    print("accuracy " + str(100*float(correct)/testCount) + "%")

    print("positive rate " + str(100*(pos)/float(pos+falseNeg)) + "%")
    print("negative rate " + str(100*(neg)/float(neg+falsePos)) + "%")

    print("falsePos " + str(100*float(falsePos)/(falsePos+neg)) + "%")
    print("falseNeg " + str(100*float(falseNeg)/(falseNeg+pos)) + "%")
    print("")


if __name__ == "__main__":
    main()
