import json
from datetime import datetime
from matplotlib import pyplot
from collections import defaultdict
import numpy as np

def main():
    print("parsing data...")

    ################
    # read in data #
    ################

    # user data
    file_name = "../data/yelp_academic_dataset_user.json"

    # dictionary of
    # key: user_id
    # value: user object
    all_users_dict = {}
    normal_users_dict = {}    # to make checking easier
    elite_users_dict = {}     # to make checking easier


    with open(file_name) as f:
        for line in f:
            data = json.loads(line)

            all_users_dict[data['user_id']] = data

            if len(data['elite']) > 0:
                elite_users_dict[data['user_id']] = data
            else:
                normal_users_dict[data['user_id']] = data

    # review data
    file_name = "../data/yelp_academic_dataset_review.json"

    # dictionary of 
    # key: user_id
    # value: review object
    all_user_reviews = defaultdict(list)
    normal_user_reviews = defaultdict(list)
    elite_user_reviews = defaultdict(list)


    with open(file_name) as f:
        for line in f:
            review = json.loads(line)
            user_id = review['user_id']

            all_user_reviews[user_id].append(review)

            if user_id in elite_users_dict:
                elite_user_reviews[user_id].append(review)
            else:
                normal_user_reviews[user_id].append(review)


    all_user_reviews_sorted = defaultdict(list)
    elite_user_reviews_sorted = defaultdict(list)
    normal_user_reviews_sorted = defaultdict(list)

    # elite_user_reviews  = normal_user_reviews
    
    print("data parsing complete...")

    ################
    # process data #
    ################

    # best fit line gradient
    eliteGradient = []      
    normalGradient = []

    for user in all_user_reviews:
        # only users with more than threshold reviews
        # if len(all_user_reviews[user]) > lower and len(all_user_reviews[user]) <= upper:
        if len(all_user_reviews[user]) > 100:
            all_user_reviews_sorted[user] = sorted(all_user_reviews[user], key= lambda k:k['date'])

            funny_count = 0
            funny_per_review = []

            useful_count = 0
            useful_per_review = []

            cool_count = 0
            cool_per_review = []

            vote_count = 0
            vote_per_review = []

            vote_color = []

            # build scatter plot data points
            count = 0
            increase = 0
            decrease = 0

            prev_avg = 0

            for i in range(len(all_user_reviews_sorted[user])):
                review = all_user_reviews_sorted[user][i]

                # useful per review
                funny_count += review['votes']['funny']
                funny_per_review.append(float(funny_count)/(i+1))

                # useful per review
                useful_count += review['votes']['useful']
                useful_per_review.append(float(useful_count)/(i+1))

                # useful per review
                cool_count += review['votes']['cool']
                cool_per_review.append(float(cool_count)/(i+1))

                # aggregate of all 3 above
                avg = float(vote_count)/(i+1)
                vote_count = funny_count + useful_count + cool_count
                vote_per_review.append(avg)

                if avg > prev_avg:
                    increase += 1
                else:
                    decrease += 1
                count += 1

                prev_avg = avg

            #################
            # best fit line #
            #################
            g, c = np.polyfit(range(len(vote_per_review)), vote_per_review, 1)

            if user in elite_users_dict:
                eliteGradient.append(g)
            else:
                normalGradient.append(g)


            ##############
            # Plot Graph #
            ##############

            if True:
                colors = ['black', 'green', 'yellow']

                funny_plt = pyplot.scatter(range(len(funny_per_review)), funny_per_review, marker='x', color=colors[0])
                useful_plt = pyplot.scatter(range(len(useful_per_review)), useful_per_review, marker='x', color=colors[1])
                cool_plt = pyplot.scatter(range(len(cool_per_review)), cool_per_review, marker='x', color=colors[2])
                vote_plt = pyplot.scatter(range(len(vote_per_review)), vote_per_review, marker='o')

                if user in elite_users_dict:
                    pyplot.title("likes/review vs review count (elite user)")
                else:
                    pyplot.title("likes/review vs review count (normal user)")

                pyplot.legend((funny_plt, useful_plt, cool_plt),
                ('funny', 'useful', 'cool'),
                scatterpoints=1,
                loc='upper left',
                ncol=3,
                fontsize=8)

                pyplot.show()

            ##############
            # Save Graph #
            ##############
            if False:
                plotname = ''.join(["../elite-votes-per-review-plots/", str(lower), '-', str(upper), '/', str(index), ".png"])
                pyplot.savefig(plotname)
                pyplot.clf()   # clear plot
                index += 1

    # end of for loop


    gradientThreshold = np.linspace(0, 0.01, 5)

    for t in gradientThreshold:
        elitePositive = 0
        eliteNegative = 0
        normalPositive = 0
        normalNegative = 0
        for g in eliteGradient:
            if g > t:
                elitePositive += 1
            else:
                eliteNegative += 1

        for g in normalGradient:
            if g > t:
                normalPositive += 1
            else:
                normalNegative += 1

        print("gradientThreshold = " + str(t))
        print("")
        print("number of elite users above threshold " + str(len(eliteGradient)))
        print("elite gradient positive " + str(100*float(elitePositive)/len(eliteGradient)) + "%")
        print("elite gradient negative " + str(100*float(eliteNegative)/len(eliteGradient)) + "%")
        print("")
        print("number of normal users above threshold " + str(len(normalGradient)))
        print("normal gradient positive " + str(100*float(normalPositive)/len(normalGradient)) + "%")
        print("normal gradient negative " + str(100*float(normalNegative)/len(normalGradient)) + "%")
        print("")
        print("")


if __name__ == "__main__":
    main()
