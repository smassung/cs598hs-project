require 'json'

num_reviews = 0
funny = 0
useful = 0
cool = 0
stars = [0, 0, 0, 0, 0]
length = 0
business_count = {}

puts ""
puts "Getting review stats..."
File.open("data/yelp_academic_dataset_review.json", 'r').each do |line|
  json = JSON.parse line
  num_reviews += 1
  funny += json["votes"]["funny"]
  useful += json["votes"]["useful"]
  cool += json["votes"]["cool"]
  stars[json["stars"] - 1] += 1
  length += json["text"].split(" ").size
  business = json["business_id"]
  business_count[business] = 0 if not business_count.has_key? business
  business_count[business] += 1
end

puts ""
puts "Found #{num_reviews} reviews."
puts "Votes:"
puts "  Avg Funny:  #{(funny.to_f / num_reviews).round(2)}"
puts "  Avg Useful: #{(useful.to_f / num_reviews).round(2)}"
puts "  Avg Cool:   #{(cool.to_f / num_reviews).round(2)}"
puts "Stars:"
for i in 0..4
  puts "  #{i + 1}: #{stars[i]}, #{(stars[i].to_f / num_reviews).round(4) * 100}%"
end
puts "Average review length: #{length / num_reviews} words"
puts ""

num_businesses = 0

puts "Getting business stats..."
categories = {}
File.open("data/yelp_academic_dataset_business.json", 'r').each do |line|
  json = JSON.parse line
  num_businesses += 1
  cats = json["categories"]
  for cat in cats
    categories[cat] = 0 if not categories.has_key? cat
    categories[cat] += 1
  end
end

puts ""
puts "Found #{num_businesses} businesses"
sorted = categories.sort_by{|k, v| v}.reverse
puts "Most reviewed categories:"
for i in 0..9
  puts "  #{i + 1}: #{sorted[i][0]} (#{sorted[i][1]} businesses)"
end

sorted = business_count.sort_by{|k, v| v}.reverse
puts "Most reviewed businesses:"
for i in 0..9
  puts "  #{i + 1}: #{sorted[i][0]} (#{sorted[i][1]} reviews)"
end

puts ""
puts "Getting user stats..."
num_users = 0
funny = 0
useful = 0
cool = 0
File.open("data/yelp_academic_dataset_user.json", 'r').each do |line|
  json = JSON.parse line
  funny += json["votes"]["funny"]
  useful += json["votes"]["useful"]
  cool += json["votes"]["cool"]
  num_users += 1
end

puts ""
puts "Found #{num_users} users."
puts "Votes:"
puts "  Avg Funny:  #{(funny.to_f / num_reviews).round(2)}"
puts "  Avg Useful: #{(useful.to_f / num_reviews).round(2)}"
puts "  Avg Cool:   #{(cool.to_f / num_reviews).round(2)}"
puts ""
