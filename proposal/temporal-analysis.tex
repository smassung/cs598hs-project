\section{Temporal Analysis}

\subsection{Average Votes-per-review Over Time}

The temporal analysis looks at how the average number of votes-per-review
varies with each review posted by an user. To gather this data, we grouped
the reviews in the Yelp dataset by users and ordered the reviews by the
date each was posted.

The goal was to try to predict whether an user is an ``Elite'' or ``Normal'' user
using the votes-per-review vs review number plot. The motivation for this was that
after processing the data, we found out that the number of votes on average were significantly
greater for Elite users compared to Normal users as show in Fig~\ref{fig:elite-vs-normal-votes}.
Thus, we decided to find out whether any trend exists on how the average number of votes
grow with each review posted by users from both categories.

\begin{figure}[t]
    \begin{center}
        \textbf{Elite vs Normal users Statistics}
        \begin{tabular}{c|c|c|c}
            & useful votes & funny votes & cool votes \\
            \hline
            elite users & 616 & 361 & 415 \\
            \hline
            normal users & 20 & 7 & 7 \\
        \end{tabular}
    \end{center}
    \caption{Average number of votes per category for elite and normal users.}
    \label{fig:elite-vs-normal-votes}
\end{figure}


On the $y$-axis, we have $\upsilon_{i}$ which is the votes-per-review after a
user posts his $i^{th}$ review. This is defined as the sum of the number of
``useful'' votes, ``cool'' votes and ``funny'' votes divided by the number of
reviews by the user. On the $x$-axis, we will have the review count.

Using the Yelp dataset, we plotted a scatter plot for each user. Visual inspection
of graphs did not show any obvious trends in how the average number of likes per review
varied with each review being posted by the user.

We then proceeded to perform a logistic regression using the following variables:

$$ P_{increase}=\frac{count(increases)}{count(reviews)} $$

$$ \mu = \frac{\sum_{i=0}^{count(reviews)}\upsilon_{i}}{count(reviews)} $$

where $count(increases)$ is the number of times the average votes-per-review increased (i.e.
$\upsilon_{i+1} > \upsilon_{i}$) after a user posts a review and $count(reviews)$ is the
number of reviews the user has made.

Both the training and testing sets consists of only users with at least one review. For each
user, we calculated the variables $P_{increase}$ and $\mu$. The training and testing
data are display in Fig~\ref{fig:lr}. 10\% of users with at least one review became part
of the training data and the remaining 90\% were used to test.

\begin{figure}[t]
    \begin{center}
        \textbf{Logistic Regression Summary}
        \begin{tabular}{c|c|c}
            & elite users & normal users \\
            \hline
            training & 2005 & 2005 \\
            \hline
            testing & 18040 & 18040 \\
        \end{tabular}
    \end{center}
    \caption{Summary of training and testing data for logistic regression.}
    \label{fig:lr}
\end{figure}

There was an accuracy of 0.69 on the testing set. The results are shown in Fig~\ref{fig:lr-results}.

\begin{figure}[t]
    \begin{center}
        \textbf{Confusion Matrix}
        \begin{tabular}{c|c|c}
            & classified as elite & classified as normal \\
            \hline
            elite & 0.64 & 0.36 \\
            \hline
            normal & 0.26 & 0.74 \\
        \end{tabular}
    \end{center}
    \caption{Summary of results for logistic regression.}
    \label{fig:lr-results}
\end{figure}

Given the overall accuracy of our model is relatively high at 0.69, we can hypothesize
that $P_{increase}$ is higher for Elite users compared to Normal users. This means that
each review that a Elite user posts tends to be a ``quality'' review that receives enough
votes to increase the running average of votes-per-review for this user. The second hypothesis
is that the mean of the running average votes-per-review for Elite users is higher than
that of Normal users. This is supported by data shown in Fig~\ref{fig:elite-vs-normal-votes}
where the average votes for Elite users are higher than Normal users.

\subsection{User Status Distribution of Business \\ Reviews over Time}
For the second part of our temporal analysis, we looked at how the fraction of elite reviews, $f_{k,m}$, for
the top $k$ businesses, vary with respect to the first $m$ number of months after the first review was posted
at time $t_{0}$.

The equation for $f_{k,m}$ is as follows:

$$ f_{k,m}=\frac{count_{<m}(elite)}  {count_{<m}(all)} $$

where $count_{<m}(elite)$ is the number of elite reviews in the first $m$ months and
$count_{<m}(all)$ is the total number of reviews in the first $m$ months.

Plotting values for $1<k<501$ at intervals of 10 and $1<m<37$, we obtained the following plot in
Fig~\ref{fig:3d_indiv1}.

\begin{figure}[t]
    \begin{center}
    \includegraphics[width=.5\textwidth]{3d_indiv1}
    \end{center}
\caption{3D plot of $f_{k,m}$ varying with $k$ and $m$.}
\label{fig:3d_indiv1}
\end{figure}

From the plot, taking a 2D slice at each month, the values are changing too irregularly to observe any trend. To smooth the
plot, at each time slice $m=m_{i}$, we took the cumulative sum of $f_{k,m_{i}}$ for each $k$ and divided it by $k$ to find the average.

$$ \overline{f_{k,m}}=\frac{\sum_{i=1}^{k}f_{i,m}}{i}$$

The results of the smoothed plot are shown in Fig~\ref{fig:3d_avg2}.

\begin{figure}[t]
    \begin{center}
    \includegraphics[width=.5\textwidth]{3d_avg2}
    \end{center}
\caption{3D plot of $\overline{f_{k,m}}$ varying with $k$ and $m$.}
\label{fig:3d_avg2}
\end{figure}

Fig~\ref{fig:3d_avg2} shows us that at each value of $m$, $\overline{f_{k,m}}$ decreases with $k$. What we can infer
from this is that the fraction of elite reviews decrease for lower ranked businesses. We can also see that for each $k$, $\overline{f_{k,m}}$ decreases
as $m$ increases. What this tells us is that elite users tend to be the ones who contribute the first few reviews of the business before
the normal users start posting reviews as well.
