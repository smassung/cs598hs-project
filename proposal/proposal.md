# To do for proposal
 - Literature review
    * Summarize the main technical contributions; identify a common thread
      amongst the papers---what ties them together?
    * What are the strengths and weaknesses of each paper?
 - Project proposal
    * Identify a promising research direction
    * Test a model or algorithm from the related work on real or simulated data
    * Propose to extend or improve the model or algorithm you tested
 - Make sure to include:
    * What exactly is the problem you are trying to solve? Be as precise as
      possible
    * What data do you plan to use?
    * Are you planning to develop a new model, a new algorithm, or both?
    * How will you evaluate your method?
    * What will your final project deliverable be?
 - Format requirements
    * 5 page double column paper 10 font, 1 inch margins (style format already
      set up in latex files)
    * References, tables, and figures

# Related papers: select some from here to review
 - [RankClus](http://www.cs.uiuc.edu/homes/hanj/pdf/edbt09_ysun.pdf)
    * clustering gives the topics and ranking gives the experts?
    * Sean
 - Choosing the Right Crowd: Expert Finding in Social Networks
    * Sean
 - [Expert Finding in a Social
   Network](http://keg.cs.tsinghua.edu.cn/jietang/publications/Zhang-et-al-Expert-Finding.pdf)
    * is this paper legit?
    * Sean
 - [Searching for Experts in the Enterprise: Combining Text and Social Network
   Analysis](http://129.34.20.8/cambridge/Technical_Reports/2007/group2007-ehrlich.pdf)
    * is this paper legit?
    * Cheng Han
 - [Hari's suggested
   paper](http://sundaram.cs.illinois.edu/pubs/2009/mmdcyouT2009.pdf)
    * Cheng Han
 - [A language modeling framework for expert
   finding](https://staff.fnwi.uva.nl/m.derijke/Publications/Files/ipm2008.pdf)
    * is this paper relevant enough?
    * Cheng Han
 - Similar tasks could be expert finding in medical forum or ranking users in a
   graph

# What is novel?
 - There is no social network component (no user-user links)

# Other questions
 - When does a user become elite?
 - What makes a user interesting?
