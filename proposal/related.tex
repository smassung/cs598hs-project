\section{Related Work}

RankClus~\cite{rankclus} integrates clustering and ranking on heterogeneous
information networks. Within each cluster, a ranking of nodes is created, so
the top-ranked nodes could be considered experts for a given cluster. For
example, consider the DBLP bibliographic network. Clusters are formed based on
authors who share coauthors, and within each cluster there is a ranking of
authoritative authors (experts in their field).

Clustering and ranking are defined to mutually enhance each other since
conditional rank is used as a clustering feature and cluster membership is used
as an object feature. In order to determine the final configuration, an
expectation-maximization algorithm is used to iteratively update cluster and
ranking assignments.

This work is relevant to our Yelp dataset if we consider clusters to be the
business categories, and experts to be domain experts. However, the Yelp
categories are not well-defined since some category labels overlap, so some
extra processing may be necessary to deal with this issue.

Expert Finding in Social Networks~\cite{choosing-right-crowd} considers
Facebook, LinkedIn, and Twitter as domains where experts reside. Instead of
labeling nodes from the entire graph as experts, a subset of candidate nodes is
considered and they are ranked according to an expertise score. These expertise
scores are obtained through link relation types defined on each social network,
such as \emph{creates, contains, annotates, owns, etc}.

To rank experts, they used a vector-space retrieval model common in information
retrieval~\cite{manning-ir-book} and evaluated with popular IR metrics such as
MAP, MRR, and NDCG~\cite{manning-ir-book}. Their vector space consisted of
resources, related entities, and expertise measures. They concluded that
profile information is a less useful determiner for expertise than their
extracted relations, and that resources created by others including the target
are also quite useful.

A paper on ``Expertise Networks''~\cite{expertise-networks} begins with a large
study on analyzing a question and answer forum; typical centrality measures such
as PageRank~\cite{pagerank} and HITS~\cite{hits} are used to initially find
expert users. Then, other features describing these expert users are defined or
extracted in order to create an ``ExpertiseRank'' algorithm, which (as far as we
can tell) is essentially PageRank. This algorithm was then evaluated by human
raters and it was found ExpertiseRank had slightly smaller errors than the other
measures (including HITS, but was \emph{not} evaluated against PageRank).

While the result of ExpertiseRank is unsurprising, we would be unable to
directly use it or PageRank since the Yelp social network is undirected;
running PageRank on an undirected network approximates degree
centrality.

A paper on Expert Language Models~\cite{expert-lm} builds two different language
models by invoking Bayes' Theorem. The conditional probability of a candidate
given a specific query is estimated by representing it using a multinomial
probability distribution over the vocabulary terms. A candidate model
$\theta_{ca}$ is inferred for each candidate $ca$, such that the probability of
a term used in the query given the candidate model is $p(t|\theta_{ca})$. For
one of the models, they assumed that the document and the candidate are
conditionally independent and for the other model, they use the probability
$p(t|d, ca)$, which is based on the strength of the co-occurrence between a term and a candidate in a particular document.

In terms of the modeling techniques used above, we can adopt a similar method whereby the candidate in the Expert Language Models~\cite{expert-lm} is a yelp user and we will determine the extent to which a $review$ characterizes an elite or normal user.

For the paper on Interesting YouTube commenters~\cite{youtube-Interesting}, the
goal is to determine a real scalar value corresponding to each conversation to
measure its interestingness. The model comprised of detecting conversational
themes using a mixture model approach, determining `interestingness' of participants and conversations based on a random walk model, and lastly, establishing the consequential impact of `interestingness' via different metrics. The paper could be useful to us for characterizing reviews and Yelp users in terms of `interestingness'. An intuitive conjecture is that `elite' users should be ones with high `interestingness' level and likewise, they should post reviews that are interesting.

\begin{figure}[t]
\begin{center}
\begin{tabular}{l|ccc}
    \hline
    \hline
    \textbf{Paper} & \textbf{Text?} & \textbf{Time?} & \textbf{Graph?} \\
    \hline
    Sun et al. 2009~\cite{rankclus} & & & $\checkmark$ \\
    Bozzon et al. 2013~\cite{choosing-right-crowd} & & & $\checkmark$ \\
    Zhang et al. 2007~\cite{expertise-networks} & & & $\checkmark$ \\
    Choudhury et al. 2009~\cite{youtube-Interesting} & $\checkmark$ & $\checkmark$ & \\
    Balog et al. 2009~\cite{expert-lm} & $\checkmark$ & & \\
    Ehrlich et al. 2007~\cite{enterprise} & $\checkmark$ & & $\checkmark$ \\
    \hline
    \hline
\end{tabular}
\end{center}
\caption{Comparison of features used in previous work.}
\label{fig:related}
\end{figure}

In summary, Fig~\ref{fig:related} shows a comparison of related work surveyed
and which aspects of the dataset they examined.
