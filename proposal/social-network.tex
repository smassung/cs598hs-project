\section{Social Network Analysis}

The Yelp social network is the user friendship graph. This data is available in
the latest version of the Yelp academic dataset. We used the graph library from
the same toolkit that was used to do the text analysis in section 3.

Since our goal is to find ``interesting'' or ``elite'' users, we use three
network centrality measures to identify central (important) nodes. We would like
to find out if elite users are more likely to be central nodes in their
friendship network. We'd also like to investigate whether the results of the
three centrality measures we investigate are correlated. Next, we briefly
summarize each measure. For a more in-depth discussion of centrality (including
the measures we use), we suggest the reader consult~\cite{data-mining}. For our
centrality calculations we considered the graph of 123,369 users that wrote at
least one review.

Degree centrality for a user $u$ is simply the degree of node $u$. In our
network, this is the same value as the number of friends. Therefore, it makes
sense that users with more friends are more important (or active) than those
that have fewer or no friends. Degree centrality can be calculated almost
instantly.

Betweenness centrality for a node $u$ essentially captures the number of
shortest paths between all pairs of nodes that pass through $u$. In this case, a
user being an intermediary between many user pairs signifies importance.
Betweenness centrality is very expensive to calculate, even using a $O(mn)$
algorithm~\cite{fast-btw}. This algorithm is part of the toolkit we used and it
took two hours to run on 3.0 GHz processors with 24 threads.

Eigenvector centrality operates under the assumption that important nodes are
connected to other important nodes. PageRank~\cite{pagerank} is a simple
extension to eigenvector centrality. If a graph is represented as an adjacency
matrix $A$, then the $(i, j)^{th}$ cell is 1 if there is an edge between $i$ and
$j$, and 0 otherwise. This notation is convenient when defining eigenvector
centrality for a node $u$ denoted as $x_u$:

$$x_u = \frac{1}{\lambda}\sum_{i=1}^n A_{iu}x_i$$

Since this can be rewritten as $A\mathbf{x} = \lambda\mathbf{x}$, we can solve
for the eigenvector centrality values with power iteration, which converges in a
small number of iterations and is quite fast. The eigenvector centralities for
the Yelp social network were calculated in less than 30 seconds.

Fig~\ref{fig:centralities} shows the comparison of the top five ranked users
based on each centrality score. The top five users of each centrality shared
some names: Walker, Gabi, and Philip in degree and betweenness; Kimquyen and
Katie in degree and eigenvector; betweenness and eigenvector shared no users in
the top five (though not shown, there are some that are the same in the range
six to ten).

The top users defined by centrality measures are almost all elite users even
though elite users only make up about 8\% of the dataset. The only exception
here is Alina from eigenvector centrality. Her other statistics look like they
fit in with the other elite users, so perhaps this could be a prediction that
Alina will be elite in the year 2015.

Fig~\ref{fig:correlations} shows the correlations between different network
features. The diagonal plots graph the distribution of values for each feature.
For example, the bottom right cell is about reviews. Most users write very few
reviews so there is a very uneven proportion of users with low review counts
demonstrated by the high bar on the left side of the plot. Picking a plot in the
lower diagonal and tracing up and right give the two features that were plotted
against another. The correlation coefficient is in the opposite cell in the
upper diagonal.

As expected, we see that degree centrality and friend count are exactly
correlated since they are equivalent. The next best correlation is $0.90$
between eigenvector centrality, friend count, and degree.

There are no strong negative correlations since all features from the Yelp
network could be used to describe an active user. The least correlated features
are review count and betweenness centrality---there is no real connection
between the number of reviews a user writes and the number of friendships they
link together. In fact, the review count itself is perhaps the least social
aspect of all other properties since it doesn't require any interaction with
other users.

The next step is to use these social network features to predict elite users. We
did a quick test using only the seven features on a balanced and unbalanced
dataset, but the classification accuracy was not promising ($52\%$ on balanced,
$30\%$ on unbalanced). It might only be possible to use these features to
predict the very top elite users. It might also be necessary to use a kernel in
the next attempt at classification if the relationship is nonlinear.

\begin{figure}[t]
    \begin{center}
        \textbf{Degree Centrality}
        \begin{tabular}{lrrrrr}
        \hline
        \textbf{Name} & \textbf{Reviews} & \textbf{Useful} & \textbf{Friends} &
        \textbf{Fans} & \textbf{Elite} \\
        \hline
        Walker   &240 & 6,166  & 2,917 & 142 & Y \\
        Kimquyen &628 & 7,489  & 2,875 & 128 & Y \\
        Katie    &985 & 23,030 & 2,561 & 1,068 & Y \\
        Philip   &706 & 4,147  & 2,551 & 86 & Y \\
        Gabi     &1,440& 12,807 & 2,550 & 420 & Y \\
        \end{tabular}
        \textbf{Betweenness Centrality}
        \begin{tabular}{lrrrrr}
        \hline
        \textbf{Name} & \textbf{Reviews} & \textbf{Useful} & \textbf{Friends} &
        \textbf{Fans} & \textbf{Elite} \\
        \hline
        Gabi   & 1,440 &12,807 & 2,550 & 420 & Y \\
        Philip & 706  &4,147  & 2,551 & 86 & Y \\
        Lindsey~~~~& 906  &7,641  & 1,617 & 348 & Y \\  % extra space for aligning
        Jon    & 230  &2,709  & 1,432 & 60 & Y \\
        Walker & 240  &6,166  & 2,917 & 142 & Y \\
        \end{tabular}
        \textbf{Eigenvector Centrality}
        \begin{tabular}{lrrrrr}
        \hline
        \textbf{Name} & \textbf{Reviews} & \textbf{Useful} & \textbf{Friends} &
        \textbf{Fans} & \textbf{Elite} \\
        \hline
        Kimquyen &628 &7,489  & 2,875 & 128  &Y \\
        Carol    &505 &2,740  & 2,159 & 163  &Y \\
        Sam      &683 &9,142  & 1,960 & 100  &Y \\
        Alina    &329 &2,096  & 1,737 & 141  &N \\
        Katie    &985 &23,030 & 2,561 & 1,068 &Y \\
        \hline
        \end{tabular}
    \end{center}
    \caption{Comparison of the top-ranked users as defined by the three
    centrality measures on the social network.}
    \label{fig:centralities}
\end{figure}

\begin{figure*}[t]
    \begin{center}
    \includegraphics[width=.8\textwidth]{correlations-800x800}
    \end{center}
\caption{Matrix plot of correlations between different network features. The
    diagonal shows the features compared and their value distribution in the
    dataset (which is very sparse). The upper diagonal shows the correlation
    coefficient for pairs of features and the lower diagonal shows the plots of
    two features with lines of best fit.}
\label{fig:correlations}
\end{figure*}
