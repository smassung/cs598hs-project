# Combine temporal, graph, metadata, and text features. It looks like all data
# files can be stored in memory for processing.
# - Temporal features are from the file votes-per-review/temporal_features.txt.
# - Graph features are from the centrality files centrality/*-centrality.txt
# - Metadata features are from the yelp user json file
# - Text features are from the index created by MeTA
#
# Output will be in libsvm format for use by MeTA or liblinear. The ids in the
# final dataset will be those in the current MeTA text dataset (balanced or
# unbalanced).
require 'set'
require 'json'

puts "Reading data..."
elite_users = Set.new
File.open("data/yelp_academic_dataset_user.json", 'r').each do |line|
  user = JSON.parse line
  elite_users.add(user["user_id"]) if user["elite"].size > 0
end

temporal = {}
File.open("votes-per-review/temporal_features.txt").each do |line|
  tokens = line.chomp.split
  id = tokens[0]
  tokens.shift
  temporal[id] = tokens
end

typography = {}
File.open("text/yelp-typography.dat").each do |line|
  tokens = line.chomp.split
  id = tokens[0]
  tokens.shift
  typography[id] = tokens
end

graph = {}
File.open("centrality/centralities.txt").each do |line|
  tokens = line.chomp.split
  id = tokens[0]
  tokens.shift
  graph[id] = tokens
end

metadata = {}
i = 0
metadata_prefix = "/home/sean/data/yelp-user-text/yelp-user-text.dat"
metadata_ids = File.open("#{metadata_prefix}.names").read.split("\n")
File.open("#{metadata_prefix}.meta").each do |line|
  metadata[metadata_ids[i]] = line.chomp.split
  i += 1
end

text_prefix = "/home/sean/projects/meta/build/yelp-user-text-fwd"
text_ids = File.open("#{text_prefix}/docids.mapping").read.split("\x00")
num_text_features = File.open("#{text_prefix}/corpus.uniqueterms").read.chomp.to_i

puts "Writing combined features..."

combined = File.open("yelp-combined.dat", 'w')
temp_only = File.open("yelp-temporal.dat", 'w')
graph_only = File.open("yelp-graph.dat", 'w')
metadata_only = File.open("yelp-metadata.dat", 'w')
type_only = File.open("yelp-typography.dat", 'w')
i = 0
File.open("#{text_prefix}/postings.index").each do |line|
  feature_offset = 0
  cur_id = text_ids[i]
  is_elite = (elite_users.include? cur_id)

  # add class label and text features
  combined << line.chomp + " "

  # add temporal info
  if is_elite
    temp_only << "elite"
  else
    temp_only << "normal"
  end
  temporal[cur_id].each_with_index do |f, oi|
    combined << "#{num_text_features + feature_offset}:#{f} "
    temp_only << " #{oi + 1}:#{f}"
    feature_offset += 1
  end

  # add typography info
  if is_elite
    type_only << "elite"
  else
    type_only << "normal"
  end
  typography[cur_id].each_with_index do |f, oi|
    combined << "#{num_text_features + feature_offset}:#{f} "
    type_only << " #{oi + 1}:#{f}"
    feature_offset += 1
  end

  # add graph info if it exists (it doesn't exist if value is 0)
  if is_elite
    graph_only << "elite"
  else
    graph_only << "normal"
  end
  if graph.has_key? cur_id
    temporal[cur_id].each_with_index do |f, oi|
      combined << "#{num_text_features + feature_offset}:#{f} "
      graph_only << " #{oi + 1}:#{f}"
      feature_offset += 1
    end
  else
    # there are three graph features
    for j in 1..3
      combined << "#{num_text_features + feature_offset}:0 "
      graph_only << " #{j}:0"
      feature_offset += 1
    end
  end

  # add metadata info
  if is_elite
    metadata_only << "elite"
  else
    metadata_only << "normal"
  end
  metadata[cur_id].each_with_index do |f, oi|
    combined << "#{num_text_features + feature_offset}:#{f} "
    metadata_only << " #{oi + 1}:#{f}"
    feature_offset += 1
  end

  combined << "\n"
  temp_only << "\n"
  type_only << "\n"
  graph_only << "\n"
  metadata_only << "\n"
  i += 1
end
