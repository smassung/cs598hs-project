import json
from datetime import datetime
from matplotlib import pyplot, dates
from mpl_toolkits.mplot3d import Axes3D
import pylab
from collections import defaultdict
import numpy as np
from datetime import datetime, date

def main():
    # top 10 most reviewed businesses

    print("parsing data...")


    # user data
    file_name = "data/yelp_academic_dataset_user.json"

    # dictionary of
    # key: user_id
    # value: user object
    normal_users_dict = {}    # to make checking easier
    elite_users_dict = {}     # to make checking easier

    with open(file_name) as f:
        for line in f:
            data = json.loads(line)

            if len(data['elite']) > 0:
                elite_users_dict[data['user_id']] = data
            else:
                normal_users_dict[data['user_id']] = data


    # review data
    file_name = "data/yelp_academic_dataset_review.json"

    business_to_reviews = defaultdict(list)


    with open(file_name) as f:
        for line in f:
            review = json.loads(line)
            user_id = review['user_id']
            business_id = review['business_id']

            if user_id in elite_users_dict:
                user_type = 'elite'
            else:
                user_type = 'normal'

            review['user_type'] = user_type
            review['date'] = datetime.strptime(review['date'], "%Y-%m-%d")

            business_to_reviews[business_id].append(review)


    # find top k businesses

    business_and_reviewCount = []

    # store in list of tuples
    for id, reviews in business_to_reviews.iteritems():
        business_and_reviewCount.append((id, len(reviews)))
   

    eliteAvgZ = []
    monthsY = []
    topkX = []


    # for the top k businesses
    for k in range(1,501, 20):
        print("preparing data for top " + str(k) + " businesses...")
        top_k = {}
        # sort by date and get the top k businesses
        top_k_temp = sorted(business_and_reviewCount, key=lambda x: x[1])[-k:]  # average of top k method
        # top_k_temp = [sorted(business_and_reviewCount, key=lambda x: x[1])[-k]]   # individual k-th method

        # convert tuple back to dictionary
        for id, reviewCount in top_k_temp:
            top_k[id] = business_to_reviews[id]

        for month in range(1,37):   #37
            # find percentage of elite user posts in first n months
            days = 30*month        # convert to days

            pElite_list = []

            # find the average elite percentage for the top k businesses
            # loop through each business
            for id, reviews in top_k.iteritems():
                # sort reviews by date
                reviews = sorted(reviews, key=lambda review: (review['date']))
                
                elite_reviews = 0
                total_reviews = 0

                # get the date of the first review
                opening_date = reviews[0]['date']
                for review in reviews:
                    review_date = review['date']
                    # find difference in days
                    delta = review_date - opening_date   


                    # if the review was posted in the first 'days'
                    if delta.days < days:
                        total_reviews += 1
                        if review['user_type'] == 'elite':
                            elite_reviews += 1
                    else:
                        break

                pElite = float(elite_reviews)/total_reviews

                pElite_list.append(pElite)

            avg = sum(pElite_list)/len(pElite_list)


            topkX.append(k)
            monthsY.append(month)
            eliteAvgZ.append(avg)

    # pyplot.scatter(topkX, eliteAvgZ)
    # pyplot.xlabel("top k businesses")
    # pyplot.ylabel("fraction of elite reviews")
    # pyplot.show()

    fig = pylab.figure()
    ax = Axes3D(fig)
    ax.scatter(topkX, monthsY, eliteAvgZ)

    ax.set_xlabel("top k businesses")
    ax.set_ylabel("months")
    ax.set_zlabel("fraction of elite reviews")

    pyplot.show()



if __name__ == "__main__":
    main()




