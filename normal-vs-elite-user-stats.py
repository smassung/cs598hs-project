import json
from datetime import datetime
from matplotlib import pyplot
from collections import defaultdict
import numpy

def main():
    print("parsing data...")

    file_name = "data/yelp_academic_dataset_user.json"
    normal_users = []
    normal_users_dict = {}    # to make checking easier
    elite_users = []
    elite_users_dict = {}     # to make checking easier

    with open(file_name) as f:
        for line in f:
            data = json.loads(line)

            if len(data['elite']) > 0:
                elite_users.append(data)
                elite_users_dict[data['user_id']] = data
            else:
                normal_users.append(data)
                normal_users_dict[data['user_id']] = data

    print("number of normal normal_users: %d" % len(normal_users))
    print("number of elite users: %d" % len(elite_users))

    # find average review count
    user_review_count = [user['review_count'] for user in normal_users]
    elite_review_count = [user['review_count'] for user in elite_users]
    user_review_avg = numpy.mean(user_review_count)
    elite_review_avg = numpy.mean(elite_review_count)

    # find average of each vote
    user_funny_vote = [user['votes']['funny'] for user in normal_users]
    elite_funny_vote = [user['votes']['funny'] for user in elite_users]
    user_funny_avg = numpy.mean(user_funny_vote)
    elite_funny_avg = numpy.mean(elite_funny_vote)

    user_useful_vote = [user['votes']['useful'] for user in normal_users]
    elite_useful_vote = [user['votes']['useful'] for user in elite_users]
    user_useful_avg = numpy.mean(user_useful_vote)
    elite_useful_avg = numpy.mean(elite_useful_vote)

    user_cool_vote = [user['votes']['cool'] for user in normal_users]
    elite_cool_vote = [user['votes']['cool'] for user in elite_users]
    user_cool_avg = numpy.mean(user_cool_vote)
    elite_cool_avg = numpy.mean(elite_cool_vote)

    # find average fans
    user_fan_count = [user['fans'] for user in normal_users]
    elite_fan_count = [user['fans'] for user in elite_users]
    user_fan_avg = numpy.mean(user_fan_count)
    elite_fan_avg = numpy.mean(elite_fan_count)

    # average number of friends
    user_friends_count = [len(user['friends']) for user in normal_users]
    elite_friends_count = [len(user['friends']) for user in elite_users]
    user_friends_avg = numpy.mean(user_friends_count)
    elite_friends_avg = numpy.mean(elite_friends_count)

    # compliment type 
    user_compliment_type_count = [len(user['compliments'].keys()) for user in normal_users]
    elite_compliment_type_count = [len(user['compliments'].keys()) for user in elite_users]
    user_compliment_type_avg = numpy.mean(user_compliment_type_count)
    elite_compliment_type_avg = numpy.mean(elite_compliment_type_count)

    # total compliment count
    user_compliment_total  = [sum(user['compliments'].values()) for user in normal_users]
    elite_compliment_total  = [sum(user['compliments'].values()) for user in elite_users]
    user_compliment_avg = numpy.mean(user_compliment_total)
    elite_compliment_avg = numpy.mean(elite_compliment_total)

    print("Review Count Avg")
    print("  normal users %d" % user_review_avg)
    print("  elite users %d" % elite_review_avg)

    print("Votes Avg:")
    print("  funny")
    print("    normal users %d" % user_funny_avg)
    print("    elite users %d" % elite_funny_avg)

    print("  Useful")
    print("    normal users %d" % user_useful_avg)
    print("    elite users %d" % elite_useful_avg)

    print("  Cool")
    print("    normal users %d" % user_cool_avg)
    print("    elite users %d" % elite_cool_avg)

    print("Average Fans")
    print("  normal users %d" % user_fan_avg)
    print("  elite users %d" % elite_fan_avg)

    print("Average number of Friends")
    print("  normal users %d" % user_friends_avg)
    print("  elite users %d" % elite_friends_avg)

    print("Compliments Type Average")
    print("  normal users %d" % user_compliment_type_avg)
    print("  elite users %d" % elite_compliment_type_avg)

    print("Compliments Total Average")
    print("  normal users %d" % user_compliment_avg)
    print("  elite users %d" % elite_compliment_avg)



    # prepare plot data
    eliteCount = []
    normalCount = []

    maxReviews = 500

    for user in elite_users_dict:
        eReviews = elite_users_dict[user]['review_count']

        if eReviews< maxReviews:
            eliteCount.append(eReviews)

    for user in normal_users_dict:
        nReviews = normal_users_dict[user]['review_count']

        if nReviews< maxReviews:
            normalCount.append(nReviews)


    # plot graph
    w0 = numpy.ones_like(eliteCount)
    w0[:len(eliteCount)/2] = 0.5
    w1 = numpy.ones_like(normalCount)
    w1[:len(normalCount)/2] = 0.5

    binwidth = 5

    pyplot.hist([eliteCount, normalCount], range(min(eliteCount), max(eliteCount) + binwidth, binwidth), label=['elite users', 'normal users'], color=['red', 'green'])
    # pyplot.hist(normalCount, range(min(reviewCount), max(reviewCount) + binwidth, binwidth), alpha=0.5, label='normal users')
    pyplot.legend(loc='upper right')
    pyplot.xlabel("number of reviews")
    pyplot.ylabel("user count")
    pyplot.show()

    

if __name__ == "__main__":
    main()
