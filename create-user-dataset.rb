require 'json'

users = {}
File.open("data/yelp_academic_dataset_review.json").each do |line|
  review = JSON.parse line
  id = review["user_id"]
  users[id] = "" if not users.has_key? id
  users[id] += review["text"].gsub(/\n/, " ") + " "
end

out = File.open("yelp-user-text.dat", 'w')
outlabels = File.open("yelp-user-text.dat.labels", 'w')
outnames = File.open("yelp-user-text.dat.names", 'w')
outmeta = File.open("yelp-user-text.dat.meta", 'w')
File.open("data/yelp_academic_dataset_user.json").each do |line|
  user = JSON.parse line
  is_elite = 0
  is_elite = 1 if user["elite"].size > 0
  out << "#{users[user["user_id"]]}\n"
  outlabels << "#{is_elite}\n"
  outnames << "#{user["user_id"]}\n"
  fvotes = user["votes"]["funny"]
  uvotes = user["votes"]["useful"]
  cvotes = user["votes"]["cool"]
  rcount = user["review_count"]
  nfriends = user["friends"].size
  nfans = user["fans"]
  stars = user["average_stars"]
  ncomps = user["compliments"].size
  outmeta << "#{fvotes} #{uvotes} #{cvotes} #{rcount} "
  outmeta << "#{nfriends} #{nfans} #{stars} #{ncomps}\n"
end
