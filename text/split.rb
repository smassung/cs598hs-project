require 'json'

puts "Reading in users..."
users = {}
File.open("../data/yelp_academic_dataset_user.json", 'r').each do |line|
  user = JSON.parse line
  users[user["user_id"]] = user
end

normal = []
elite = []
puts "Reading in reviews..."
File.open("../data/yelp_academic_dataset_review.json", 'r').each do |line|
  review = JSON.parse line
  user = users[review["user_id"]]
  if user["elite"].size > 0
    elite << review["text"].gsub(/\n/, " ")
  else
    normal << review["text"].gsub(/\n/, " ")
  end
end

puts "Found #{elite.size} elite reviews"
puts "Found #{normal.size} normal reviews"
normal.shuffle!
normal = normal[0, elite.size]

out_norm = File.open("normal.dat", 'w')
for review in normal
  out_norm << "#{review}\n"
end

out_elite = File.open("elite.dat", 'w')
for review in elite
  out_elite << "#{review}\n"
end
