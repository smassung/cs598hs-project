# Dataset creation
- Found 329624 elite reviews
- Found 795834 normal reviews

# Entire dataset (even number of classes)
- Number of documents: 659248
- Avg Doc Length: 81.8204
- Unique Terms: 164311
- Raw data size: 480MB
- Indexed data size: 81MB

# Elite only
- Number of documents: 329624
- Avg Doc Length: 98.7653
- Unique Terms: 125137
- Raw data size: 291MB
- Indexed data size: 46MB

# Normal only
- Number of documents: 329624
- Avg Doc Length: 64.8756
- Unique Terms: 95428
- Raw data size: 190MB
- Indexed data size: 37MB

# Classification accuracy: predicting elite based on text

SVM with 5-fold CV, baseline 50% accuracy.

Confusion matrix:

       | elite |  normal
-------|-------|---------
elite  | 0.665 |  0.335
normal | 0.252 |  0.748

Breakdown:

Class   |   F1 Score |  Precision | Recall
--------|------------|------------|------------
elite   |   0.694    |  0.665     | 0.725
normal  |   0.718    |  0.748     | 0.691
Total   |   0.706    |  0.706     | 0.708

659245 predictions attempted, overall accuracy: 0.706

# Simple language model comparison

See part 3 of [this
assignment](http://web.engr.illinois.edu/~massung1/su14-cs410/mps/mp1.html) for
details.

### Top 20 words in background LM

```
the    4783140
and    3006865
a      2470139
i      2442618
to     2165580
was    1542434
of     1481384
is     1108078
for    1062025
it     1050386
in     1038492
that   792292
my     786945
with   750461
but    743548
this   707595
you    698054
we     697965
they   645041
on     637913
```

### Top 20 words in smoothed "normal" LM:

```
gorsek         2.4699301315724624
forks)         2.4486376304382174
yu-go          2.444008825843816
sabroso        2.4389592208317423
(\****         2.4389592208317423
eloff          2.4334287010566134
-/+            2.4206211815773684
jeph           2.4206211815773684
~deirdra       2.4048001281030063
ruffin'        2.4048001281030063
josefa         2.4048001281030063
ubox           2.4048001281030063
waite          2.3954063776026038
again!!!!!!    2.3954063776026038
optionz        2.3954063776026038
ecig           2.3954063776026038
nulook         2.3954063776026038
gtr            2.3954063776026038
shiba          2.3954063776026038
kenta          2.3847601270354812
```

### Top 20 words in smoothed "elite" LM:

```
\uuu/          1.6691386589336246
aloha!!!       1.6633486031898093
**recommendations** 1.6576424330244928
meter:         1.6563536925517817
**summary**    1.6556270622852531
carin          1.6533884319132235
no1dp          1.652769880311311
(lyrics        1.6525116350175122
friends!!!!!   1.6514106945544758
**ordered**    1.6498600741840022
8/20/2011      1.6491769023106337
rickie         1.6435468341042165
kuge           1.6418553622337833
;]]]           1.639960913738898
#365           1.639960913738898
~g             1.6385663891523854
*price         1.6362421815081976
visits):       1.6362421815081976
r\_            1.6362421815081976
ik             1.6345125386101973
```

While the normal LM does not give too much information, the elite LM shows that
elite users are more likely to segment their reviews into different sections,
discussing different aspects of the business. For example, recommendations,
summary, ordered, price, etc.

# Topic Modeling
