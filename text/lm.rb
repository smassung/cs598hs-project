def inc_hash(ht, elem)
  ht[elem] = 0 if not ht.has_key? elem
  ht[elem] += 1
end

def top_ten(ht)
  i = 0
  for w in ht.sort_by {|k, v| v}.reverse
    break if i >= 20
    printf "%-14s %s\n", w[0], w[1]
    i += 1
  end
end

def total_counts(ht)
  total = 0
  ht.each { |k, v| total += v }
  total
end

def get_words(line)
  line.downcase!
  line.split
end

if ARGV.size != 1
  puts "usage: ruby lm.rb [dataset]"
  exit
end

# compute background LM

freqs = {}
lines = File.open(ARGV[0], 'r').readlines
for line in lines
  words = get_words(line)
  words.each { |w| inc_hash(freqs, w) }
end

puts "\nTop 10 words in Background LM:"
top_ten(freqs)

# topic language model

normal_freqs = {}
elite_freqs = {}
count = 0
for line in lines
  words = get_words(line)
  if count > lines.size / 2
    words.each { |e| inc_hash(normal_freqs, e) }
  else
    words.each { |e| inc_hash(elite_freqs, e) }
  end
  count += 1
end

puts "\nTop 10 words in \"normal\" LM:"
top_ten(normal_freqs)
puts "\nTop 10 words in \"elite\" LM:"
top_ten(elite_freqs)

# normalized topic LM

def normalize(background, normal_freqs, elite_freqs)
  normal_probs = {}
  elite_probs = {}
  normal_count = total_counts(normal_freqs)
  elite_count = total_counts(elite_freqs)

  normal_freqs.each { |k, v| normal_probs[k] = v.to_f / normal_count }
  elite_freqs.each { |k, v| elite_probs[k] = v.to_f / elite_count }

  norm_normal_probs = {}
  normal_probs.each do |k, v|
    norm_normal_probs[k] = normal_probs[k] / background[k]
  end

  norm_elite_probs = {}
  elite_probs.each do |k, v|
    norm_elite_probs[k] = elite_probs[k] / background[k]
  end

  puts "\nTop 10 words in normalized \"normal\" LM:"
  top_ten(norm_normal_probs)
  puts "\nTop 10 words in normalized \"elite\" LM:"
  top_ten(norm_elite_probs)
end

puts "\nWith unsmoothed background LM:\n"
total_count = total_counts(freqs)
corpus_probs = {}
freqs.each do |k, v|
  corpus_probs[k] = v.to_f / total_count
end
normalize(corpus_probs, normal_freqs, elite_freqs)

# smoothed background LM

puts "\nWith smoothed background LM:\n"
smooth_corpus_probs = {}
corpus_probs.each do |k, v|
  smooth_corpus_probs[k] = (freqs[k] + 1).to_f / (total_count + corpus_probs.size)
end
normalize(smooth_corpus_probs, normal_freqs, elite_freqs)
