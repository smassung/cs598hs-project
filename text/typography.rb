require 'json'

def paragraphs(text)
  m = /\n+\n/.match(text)
  return 0 if m.nil?
  m.size
end

def bullet_points(text)
  m = /\n(\*|-)/.match(text)
  return 0 if m.nil?
  m.size
end

def sentiment(text, map)
  valence = 0.0
  for word in text.split
    word.downcase!
    valence += map[word] if map.has_key? word
  end
  valence
end

def num_all_caps(text)
  num = 0
  for word in text.split
    num += 1 if /[A-Z]/.match(word)
  end
  num
end

def bad_punc(text)
  m = /(\?|!|\.) +[a-z]/.match(text)
  return 0 if m.nil?
  m.size
end

puts "Reading sentiment valences..."
sent_map = {}
File.open("valences.txt").each do |line|
  tokens = line.chomp.split
  sent_map[tokens[0]] = tokens[1].to_f
end

puts "Getting all reviews for each user..."
user_reviews = {}
File.open("../data/yelp_academic_dataset_review.json").each do |line|
  review = JSON.parse line
  uid = review["user_id"]
  user_reviews[uid] = [] if not user_reviews.has_key? uid
  user_reviews[uid] << review["text"]
end

puts "Generating text features..."
out = File.open("yelp-typography.dat", 'w')
for id, reviews in user_reviews
  avg_review_length = 0.0
  avg_sentiment = 0.0
  rate_paragraphs = 0.0
  rate_bullet_points = 0.0
  all_caps = 0.0
  bad_punc = 0.0
  for review in reviews
    length = review.split.size
    avg_review_length += length
    rate_paragraphs += paragraphs(review)
    rate_bullet_points += bullet_points(review)
    all_caps += num_all_caps(review).to_f / length
    avg_sentiment += sentiment(review, sent_map)
    bad_punc += bad_punc(review)
  end
  avg_review_length /= reviews.size
  avg_sentiment /= reviews.size
  rate_paragraphs /= reviews.size
  rate_bullet_points /= reviews.size
  all_caps /= reviews.size
  bad_punc /= reviews.size
  out << "#{id} #{avg_review_length} #{avg_sentiment} #{rate_paragraphs} #{rate_bullet_points} #{all_caps} #{bad_punc}\n"
end
